<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'RegisteredController@index');

Route::get('/export_excel_cierre', 'ExcelController@export');
Route::get('/excel_no_matriculados', 'ExcelController@exportNoMatriculados');
Route::get('/metas', 'TargetController@index');

//Route::get('/importar_excel', 'ExcelController@import');
Route::get('/getDataChartHeadquarter', 'RegisteredController@dataChart');
Route::get('/getDataChartCareer', 'RegisteredController@dataChartCareer');
Route::get('/getDataChartFaculty', 'RegisteredController@dataChartFaculty');
Route::get('/getDataChartDate', 'RegisteredController@dataChartDate');
Route::get('/getDataChartWorkingDay', 'RegisteredController@dataChartWorkingDay');
Route::get('/getDataChartOlderNew', 'RegisteredController@dataChartOlderNew');

Route::get('/sede', 'TargetController@FilterByHeadquarter');
