<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetentionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retention', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('matricula');
            $table->string('rutalumno');
            $table->string('nombre_alumno');
            $table->integer('cod_carrera');
            $table->string('carrera');
            $table->string('jornada');
            $table->string('sede');
            $table->string('facultad');
            $table->string('tipo_carrera');
            $table->string('tipo_beneficio');
            $table->string('tipo_estudiante');
            $table->string('estado_matricula');
            $table->string('jefe_carrera');
            $table->string('aprobacion_semestre_uno');
            $table->string('rango_aprobacion');
            $table->string('aprobacion_semestre_dos');
            $table->string('rango_aprobacion_dos');
            $table->string('asistencia_uno');
            $table->string('rango_asistencia_uno');
            $table->string('asistencia_dos');
            $table->string('rango_asistencia_dos');
            $table->string('deuda_total');
            $table->integer('mora');
            $table->string('codigo_bloqueo');
            $table->string('estado_final');
            $table->string('status_habilitado');
            $table->string('fono_1');
            $table->string('fono_2');
            $table->string('fono_3');
            $table->string('fono_4');
            $table->string('fono_5');
            $table->string('fono_6');
            $table->string('fono_7');
            $table->string('cod_sede');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
