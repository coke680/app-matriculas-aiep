<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTargetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target', function (Blueprint $table) {
            $table->increments('id');
            $table->string('origen');
            $table->integer('cod_carrera');
            $table->string('carrera');
            $table->string('jornada');
            $table->integer('cod_sede');
            $table->string('sede');
            $table->string('escuela');
            $table->integer('meta_alumnos');
            $table->string('tipo_carrera');
            $table->string('carrera_resumen');
            $table->string('origen_retencion');
            $table->string('jefe_carrera');
            $table->string('rut_jefe_carrera');
            $table->string('coordinador_1');
            $table->string('rut_coordinador_1');
            $table->string('coordinador_2');
            $table->string('rut_coordinador_2');
            $table->string('coordinador_3');
            $table->string('rut_coordinador_3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('target');
    }
}
