<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisteredTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registered', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('matricula');
            $table->string('origen');
            $table->string('rutalumno');
            $table->string('nombrealumno');
            $table->integer('cod_carrera');
            $table->string('carrera');
            $table->string('facultad');
            $table->string('fecha');
            $table->integer('cod_sede');
            $table->string('sede_carrera');
            $table->string('jornada');
            $table->string('usuariomat');
            $table->string('rutaceptante');
            $table->string('nomaceptante');
            $table->string('tipo_arancel');
            $table->integer('agno_ingreso');
            $table->integer('cod_carrera_anterior');
            $table->string('nombre_carrera');
            $table->string('jornada_programa');
            $table->string('sede_anterior');
            $table->integer('cod_sede_anterior');
            $table->string('jefe_carrera');
            $table->string('rut_jefe_carrera');
            $table->string('escuela_anterior');
            $table->string('estado_origen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registered');
    }
}