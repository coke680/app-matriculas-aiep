@extends ('app')

@section('content')

	<div class="row tile_count text-center">
		<div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-clock-o"></i> Días para el término del proceso</span>
			<div class="count" id="clock"></div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-user"></i> Total Matriculados hasta el momento</span>
			<div class="count">{{ $total_registered }}</div>
			{{-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>2% </i> Desde ayer</span> --}}
		</div>
		<div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-flag-checkered "></i> Meta total cumplida</span>
			<div class="count">{{ $percentage }} %</div>
			{{-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>2% </i> Desde ayer</span> --}}
		</div>
	</div>

    <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="x_panel">
          	<div class="x_title">
            	<h2 class="text-center"><small >Matriculados por sede V/S Metas</small></h2>
            	<div class="clearfix"></div>            	
          	</div>
          	<canvas id="myChart-bar" width="100"></canvas>
        </div>
    </div>
    
    <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="x_panel">
          	<div class="x_title">
            	<h2 class="text-center"><small >Matriculados clasificados por escuela</small></h2>
            	<div class="clearfix"></div>            	
          	</div>
          	<canvas id="myChartFaculty" width="100"></canvas>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
          	<div class="x_title">
            	<h2><small>Matriculados por jornada V/S Metas</small></h2>
            	<div class="clearfix"></div>            	
          	</div>
          	<canvas id="myChartWorkingDay" width="100"></canvas>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
          	<div class="x_title">
            	<h2><small>Matriculados antiguos/nuevos V/S Metas</small></h2>
            	<div class="clearfix"></div>            	
          	</div>
          	<canvas id="myChartOlderNew" width="100"></canvas>
        </div>
    </div>

    <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="x_panel">
          	<div class="x_title">
            	<h2><small>Total de los últimos 30 días</small></h2>
            	<div class="clearfix"></div>            	
          	</div>
          	<canvas id="myChart-line" width="100"></canvas>
        </div>
    </div>


    <div class="clearfix"></div>

    <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h4>Avance de las metas por sede</h4>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <!-- start project list -->
            <table class="table table-striped table-bordered projects text-center">
              	<thead>
                	<tr>
	                    <th style="width: 10%" class="text-center">Sede</th>
	                  	<th style="width: 5%" class="text-center">Meta</th>
	                  	<th style="width: 5%" class="text-center">Matriculados</th>
	                  	<th style="width: 5%" class="text-center">Restante</th>
	                  	<th style="width: 5%" class="text-center">% Avance Matriculados</th>
	                  	<th style="width: 5%" class="text-center">Habilitados</th>
	                  	<th style="width: 5%" class="text-center">% Avance Habilitados</th>
                	</tr>
             	 </thead>
              	<tbody>         	
              	
              	@foreach ($data as $register)
					<tr>
	                    <td>
	                    	<a>{{ $register['sede'] }}</a>	                    	
	                  	</td>
	                  	<td>
	                    	<a>{{ $register['suma'] }}</a>
	                  	</td>         
	                  	<td>
	                    	<a>{{ $register['count'] }}</a>
	                  	</td>
	                  	<td>
	                    	<a>{{ $register['suma'] - $register['count']  <= 0 ? "0" : $register['suma'] - $register['count']  }}</a>
	                  	</td>
	                  	
						{{! $percent = (($register['count'] * 100) / $register['suma']) }}
						{{! $percent = number_format((float)$percent, 1, '.', '') }}

	                  	<td class="project_progress">
	                    	<div class="progress progress_sm">
	                      		<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="100" aria-valuenow="56" style="width: {{$percent}}%;"></div>
	                    	</div>
	                    	<small>{{$percent}} % Completo</small>
	                 	</td>
	                  	<td>
	                    	<a>{{ $register['count_habilitado'] }}</a>
	                  	</td>

	                  	{{! $percent_habilitados = (($register['count_habilitado'] * 100) / $register['suma']) }}
						{{! $percent_habilitados = number_format((float)$percent_habilitados, 1, '.', '') }}

	                 	<td class="project_progress">
	                    	<div class="progress progress_sm">
	                      		<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="100" aria-valuenow="56" style="width: {{$percent_habilitados}}%;"></div>
	                    	</div>
	                    	<small>{{$percent_habilitados}} % Completo</small>
	                 	</td>

	                 	{{! $sum_total_meta[] = $register['suma'] }}
	                 	{{! $sum_habilitados[] = $register['count_habilitado'] }}

	                </tr>
				@endforeach                

					<tr class="success">
					
						{{! $total_meta = array_sum($sum_total_meta) }}
						{{! $total_habilitado = array_sum($sum_habilitados) }}

						<td><strong>TOTALES</strong></td>
						<td>
	                    	<a>{{ $total_meta }}</a>							
	                  	</td>
	                  	<td>
	                    	<a>{{ $total_registered }}</a>
	                  	</td>
	                  	<td>
	                    	<a>{{ $total_meta - $total_registered }}</a>
	                  	</td>
	                  	<td class="project_progress">
	                    	<div class="progress progress_sm">
	                      		<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="100" aria-valuenow="56" style="width: {{ $percentage }}%"></div>
	                    	</div>
	                    	<small>{{ $percentage }} % Completo</small>
	                 	</td>
	                 	<td>
	                    	<a>{{ $total_habilitado }}</a>
	                  	</td>
	                  	
	                  	{{! $total_habilitados = (($total_habilitado * 100) / $total_meta) }}
						{{! $total_habilitados = number_format((float)$total_habilitados, 1, '.', '') }}

	                 	<td class="project_progress">
	                    	<div class="progress progress_sm">
	                      		<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="100" aria-valuenow="56" style="width: {{$total_habilitados}}%;"></div>
	                    	</div>
	                    	<small>{{$total_habilitados}} % Completo</small>
	                 	</td>

	                <tr>
				</tbody>
            </table>
            <!-- end project list -->

          </div>
        </div>
    </div>
	
@endsection

@push('line-graph')

<script>
	function drawBarValues() {
	  // render the value of the chart above the bar
	  var ctx = this.chart.ctx;
	  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
	  ctx.fillStyle = this.chart.config.options.defaultFontColor;
	  ctx.textAlign = 'center';
	  ctx.textBaseline = 'bottom';
	  this.data.datasets.forEach(function (dataset) {
	    for (var i = 0; i < dataset.data.length; i++) {
	      if(dataset.hidden === true && dataset._meta[Object.keys(dataset._meta)[0]].hidden !== false){ continue; }
	      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
	      if(dataset.data[i] !== null){
	        ctx.fillText(dataset.data[i], model.x - 1, model.y - 5);
	      }
	    }
	  });
	}

	function drawBarValuesHorizontal() {
	  // render the value of the chart above the bar
	  var ctx = this.chart.ctx;
	  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
	  ctx.fillStyle = this.chart.config.options.defaultFontColor;
	  ctx.textAlign = 'center';
	  ctx.textBaseline = 'bottom';
	  this.data.datasets.forEach(function (dataset) {
	    for (var i = 0; i < dataset.data.length; i++) {
	      if(dataset.hidden === true && dataset._meta[Object.keys(dataset._meta)[0]].hidden !== false){ continue; }
	      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
	      if(dataset.data[i] !== null){
	        ctx.fillText(dataset.data[i], model.x + 20, model.y + 10);
	      }
	    }
	  });
	}
</script>
<script>

$.getJSON("/getDataChartDate", function (result) {

    var labels = [], data=[];

    for (var i = 0; i < result.length; i++) {
        labels.push(result[i].fecha);
        data.push(result[i].count);
    }

	var ctx = document.getElementById("myChart-line").getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        labels: labels,
	        datasets: [{
	            label: 'Cantidad de matriculados',
	            data: data,
	            backgroundColor: 'rgba(54, 162, 235, 0.2)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        }]
	    },
	    options: {
	    	animation: {
		    	onProgress: drawBarValues,
		    	onComplete: drawBarValues
		    },
		    hover: { 
		    	animationDuration: 0 
		    },
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }],
	            xAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    autoSkip: false,
	                    fontSize: 10
	                }
	            }]
	        }
	    },
	});
});

</script>

<script>

$.getJSON("/getDataChartHeadquarter", function (result) {

    var labels = [], count = [], suma = [];

    registered = result.data;

    for (var i = 0; i < registered.length; i++) {
        labels.push(registered[i].sede);
        count.push(registered[i].count);
        suma.push(registered[i].suma);
    }

	var ctx = document.getElementById("myChart-bar").getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: labels,
	        datasets: [{
	            label: 'Cantidad de matriculados',
	            data: count,
	            backgroundColor: 'rgba(54, 162, 235, 0.2)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        },
	        {
	            label: 'Meta 2018',
	            data: suma,
	            backgroundColor: 'rgba(54, 162, 235, 0.6)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        }]
	    },
	    options: {
	    	animation: {
		    	onProgress: drawBarValues,
		    	onComplete: drawBarValues
		    },
		    hover: { 
		    	animationDuration: 0 
		    },
	        scales: {
	            yAxes: [{
	            	//stacked: true,
	                ticks: {
	                    beginAtZero:true
	                }
	            }],
	            xAxes: [{
	            	//stacked: true,
	                ticks: {
	                    beginAtZero:true,
	                    autoSkip: false,
	                    fontSize: 10
	                }
	            }]
	        }
	    }
	});
});

</script>

<script>

$.getJSON("/getDataChartFaculty", function (result) {

    var labels = [], data = [], target = [], sum_target = [];

    registered_faculty = result.registered_faculty;
    target_faculty = result.target_faculty;

    for (var i = 0; i < registered_faculty.length; i++) {
        labels.push(registered_faculty[i].facultad);
        data.push(registered_faculty[i].count);
    }

    for (var i = 0; i < target_faculty.length; i++) {
        sum_target.push(target_faculty[i].suma);
        target.push(target_faculty[i].escuela);
    }

	var ctx = document.getElementById("myChartFaculty").getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'horizontalBar',
	    data: {
	        labels: target,
	        datasets: [{
	            label: 'Cantidad de matriculados',
	            data: data,
	            backgroundColor: 'rgba(54, 162, 235, 0.2)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        },
	        {
	            label: 'Meta 2018',
	            data: sum_target,
	            backgroundColor: 'rgba(54, 162, 235, 0.6)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        }]
	    },
	    options: {
	    	animation: {
		    	onProgress: drawBarValuesHorizontal,
		    	onComplete: drawBarValuesHorizontal
		    },
		    hover: { 
		    	animationDuration: 0 
		    },
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    fontSize: 9
	                }
	            }],
	            xAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    autoSkip: false,

	                }
	            }]
	        }
	    }
	});
});

</script>

<script>

$.getJSON("/getDataChartWorkingDay", function (result) {

    var labels = [], data = [], target = [], sum_target = [];

    working_day = result.working_day;
    working_day_target = result.working_day_target;

    for (var i = 0; i < working_day.length; i++) {
        labels.push(working_day[i].jornada_programa);
        data.push(working_day[i].suma_jornada);
    }

    for (var i = 0; i < working_day_target.length; i++) {
        target.push(working_day_target[i].jornada);
        sum_target.push(working_day_target[i].suma_jornada);
    }
   
	var ctx = document.getElementById("myChartWorkingDay").getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: target,
	        datasets: [{
	            label: 'Cantidad de matriculados',
	            data: data,
	            backgroundColor: 'rgba(54, 162, 235, 0.2)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        },
	        {
	            label: 'Meta 2018',
	            data: sum_target,
	            backgroundColor: 'rgba(54, 162, 235, 0.6)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        }]
	    },
	    options: {
	    	animation: {
		    	onProgress: drawBarValues,
		    	onComplete: drawBarValues
		    },
		    hover: { 
		    	animationDuration: 0 
		    },
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    fontSize: 9
	                }
	            }],
	            xAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    autoSkip: false,

	                }
	            }]
	        }
	    }
	});
});

</script>

<script>

$.getJSON("/getDataChartOlderNew", function (result) {

    var labels = [], count = [], target = [], sum_target = [];

    older = result.older;
    older_new_target = result.older_new_target;

    for (var i = 0; i < older.length; i++) {
        labels.push(older[i].estado_origen);
        count.push(older[i].count);
    }

    for (var i = 0; i < older_new_target.length; i++) {
        target.push(older_new_target[i].origen);
        sum_target.push(older_new_target[i].suma);
    }

	var ctx = document.getElementById("myChartOlderNew").getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: target,
	        datasets: [{
	            label: 'Cantidad de matriculados',
	            data: count,
	            backgroundColor: 'rgba(54, 162, 235, 0.2)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        },
	        {
	            label: 'Meta 2018',
	            data: sum_target,
	            backgroundColor: 'rgba(54, 162, 235, 0.6)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        }]
	    },
	    options: {
	    	animation: {
		    	onProgress: drawBarValues,
		    	onComplete: drawBarValues
		    },
		    hover: { 
		    	animationDuration: 0 
		    },
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    fontSize: 9
	                }
	            }],
	            xAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    autoSkip: false,

	                }
	            }]
	        }
	    }
	});
});

</script>
@endpush