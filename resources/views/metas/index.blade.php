@extends ('app')

@section('content')

	<div class="col-lg-12">
	  	<div class="">
		    <div class="x_panel ui-ribbon-container">
			    <div class="x_title">
			      <h4><i class="fa fa-flag fa-space"></i> Metas propuestas para el año 2018</h4>
			    </div>
			    <div class="x_content">
					<div class="row table-responsive">
						<table class="table table-hover table-bordered" id="targetTable">
							<thead>
								<tr>
									<th>Código carrera</th>
									<th>Carrera</th>
									<th>Jornada</th>
									<th>Sede</th>
									<th>Escuela</th>
									<th>Meta</th>
									<th>Tipo carrera</th>
									<th>Jefe carrera</th>
								</tr>
							</thead>
							<tbody >
							@foreach($metas as $meta)
								<tr>
									<td>{{ $meta->cod_carrera }}</td>
									<td>{{ $meta->carrera }}</td>
									<td>{{ $meta->jornada }}</td>
									<td>{{ $meta->sede }}</td>
									<td>{{ $meta->escuela }}</td>
									<td class="">{{ $meta->meta_alumnos }}</td>
									<td>{{ $meta->tipo_carrera }}</td>
									<td>{{ $meta->jefe_carrera }}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
			    </div>
		    </div>
	  	</div>
	</div>

@endsection

@push('datatable')
  <script>
    $(document).ready(function(){

        $('#targetTable').DataTable({

            responsive: true,
            processing: true,

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "search": "Filtrar:",
                "zeroRecords": "No encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(Filtrando _MAX_ registros)",
                "paginate": {
                    "previous": "Página anterior",
                    "next": "Página siguiente"
                  },
            },

            fixedColumns: true,

        });

    });


</script>
@endpush