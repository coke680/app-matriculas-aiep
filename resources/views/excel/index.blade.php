@extends ('app')

@section('content')
<div class="col-lg-12">
  	<div class="">
	    <div class="x_panel ui-ribbon-container">
		    <div class="x_title">
		      <h4>Subir cierre comercial (Excel)</h4>
		    </div>
		    <div class="x_content">
				<p>Antes de subir algún archivo, favor de leer las siguientes instrucciones:</p><br>
				<p>El formato del arhivo será en Excel (.xlsx)</p>

				<form action="" method="POST" role="form">
					<legend>Adjuntar aquí:</legend>
				
					<div class="form-group">
						<label for="">label</label>
						<input type="text" class="form-control" id="" placeholder="Input field">
					</div>
				
					
				
					<button type="submit" class="btn btn-primary">Subir archivo</button>
				</form>
		    </div>
	    </div>
  	</div>
</div>
	
@endsection

@push('maps')

@endpush