<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="Jorge Luis Martínez">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Rematrícula 2018</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="/css/custom.css">

    <link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
                <a href="/" class="site_title text-center">
                    <img src="/images/logo.png" width="60%" class="img-responsive logo-principal col-lg-offset-2" alt="">
                </a>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                    <li><a href="/"><i class="fa fa-bar-chart-o"></i> Dashboard</a></li>
                  <li><a href="/metas"><i class="fa fa-flag"></i> Metas</a>
                  </li>
                </ul>
              </div>
              <div class="container">
                
              </div>
              <div class="col-md-12 col-sm-6 col-xs-12">
                  <div class="x_panel">        
                      <form action="/sede" method="GET" role="form">
                        <legend><i class="fa fa-filter"></i> Filtros</legend>
                      
                        <div class="form-group">
                          <label for="">Sede</label>
                          <select class="form-control" required id="cod" name="cod">
                            <option value="">- Seleccione sede -</option>                  
                            @foreach(DB::table('target')->select('cod_sede', 'sede')->distinct()->orderBy('sede')->get() as $headquarter)
                                <option value="{{ $headquarter->cod_sede }}">{{ $headquarter->sede }} - {{ $headquarter->cod_sede }}</option>
                            @endforeach
                        </select>
                        </div>           
                        <button type="submit" class="btn btn-success">Filtrar</button>
                      </form>
                  </div>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li>
                  <a href='/export_excel' target="_blank">
                    <i class="fa fa-cloud-download"></i> Descargar Matriculados
                  </a>
                </li>
                <li>
                  <a href='/excel_no_matriculados' target="_blank">
                    <i class="fa fa-cloud-download"></i> Descargar "No Matriculados"
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="page-title">
              <div class="title_left">
                <h3><i class="fa fa-bar-chart-o fa-space"></i> Dashboard | Gestión de matriculados</h3>
              </div>
            </div>

            <div class="clearfix"></div>
            
            <div class="row">
                @yield('content')
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gestión de Matriculados | AIEP 2018 | <i class="fa fa-university"></i> Departamento de Análisis Institucional
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>    
    <!-- Custom Theme Scripts -->
    <script src="/js/jquery.countdown.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>

    <script>
        $(document).ready(function(){
            var url = window.location;
            $('ul.nav a').filter(function() {
                return this.href == url;
            }).parent().addClass('active open');

            $('#clock').countdown('2018/03/31', function(event) {
              $(this).html(event.strftime('%D días <span> %H:%M:%S hrs</span>'));
            });
        });
    </script>

    @stack('datatable')
    @stack('line-graph')

  </body>
</html>
