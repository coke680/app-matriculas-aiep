@extends ('app')

@section('content')

	<div class="col-lg-12">
	  	<div class="">
		    <div class="x_panel ui-ribbon-container">
			    <div class="x_title">
			      <h4><i class="fa fa-flag fa-space"></i> Información Sede {{ $name[0]->sede }}</h4>
			    </div>
			    <div class="x_content">
					<div class="row table-responsive">
						<table class="table table-hover table-bordered" id="targetTable">
							<thead>
								<tr>									
									<th class="text-center">Nombre</th>
									<th class="text-center">Meta</th>
									<th class="text-center">Matriculados</th>
									<th class="text-center">Restantes</th>
									<th class="text-center">Avance Matriculados</th>
									<th class="text-center">Habilitados</th>
									<th class="text-center">Avance habilitados</th>
								</tr>
							</thead>
							<tbody >

							@foreach($data as $item)
								<tr>
									<td>{{ $item['jefe_carrera']}}</td>
									<td>{{ $item['suma'] }}</td>
									<td>{{ $item['count'] }}</td>
									<td>	
				                    	<a>{{ $item['suma'] - $item['count']  <= 0 ? "0" : $item['suma'] - $item['count']  }}</a>
				                  	</td>
									{{-- Esto no se debe hacer acá, se hace en el controlador --}}	
									{{! $percent = (($item['count'] * 100) / $item['suma']) }}
									{{! $percent = number_format((float)$percent, 1, '.', '') }}

				                  	<td class="project_progress">
				                    	<div class="progress progress_sm">
				                      		<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="100" aria-valuenow="56" style="width: {{$percent}}%;"></div>
				                    	</div>
				                    	<small>{{$percent}} % Completo</small>
				                 	</td>
				                 	<td>{{ $item['count_habilitados'] }}</td>
									
				                 	{{! $percent_habilitados = (($item['count_habilitados'] * 100) / $item['suma']) }}
									{{! $percent_habilitados = number_format((float)$percent_habilitados, 1, '.', '') }}

				                 	<td class="project_progress">
				                    	<div class="progress progress_sm">
				                      		<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="100" aria-valuenow="56" style="width: {{$percent_habilitados}}%;"></div>
				                    	</div>
				                    	<small>{{$percent_habilitados}} % Completo</small>
				                 	</td>				                 	

								</tr>
							@endforeach
							</tbody>

						</table>
					</div>
			    </div>
		    </div>
	  	</div>
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
          	<div class="x_title">
            	<h2 class="text-center"><small >Cantidad de matriculados por jornada</small></h2>
            	<div class="clearfix"></div>            	
          	</div>
          	<canvas id="myChartWorkingDay" width="100"></canvas>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="x_panel">
          	<div class="x_title">
            	<h2 class="text-center"><small >Cantidad de matriculados nuevos y antiguos</small></h2>
            	<div class="clearfix"></div>            	
          	</div>
          	<canvas id="myChartOrlderNew" width="100"></canvas>
        </div>
    </div>

@endsection

@push('line-graph')

<script>
	function drawBarValues() {
	  // render the value of the chart above the bar
	  var ctx = this.chart.ctx;
	  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
	  ctx.fillStyle = this.chart.config.options.defaultFontColor;
	  ctx.textAlign = 'center';
	  ctx.textBaseline = 'bottom';
	  this.data.datasets.forEach(function (dataset) {
	    for (var i = 0; i < dataset.data.length; i++) {
	      if(dataset.hidden === true && dataset._meta[Object.keys(dataset._meta)[0]].hidden !== false){ continue; }
	      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
	      if(dataset.data[i] !== null){
	        ctx.fillText(dataset.data[i], model.x - 1, model.y - 5);
	      }
	    }
	  });
	}

</script>

<script>
	//gráficos por jornada
    var labels = [], count = [], sum_target = [];

    working_day = <?php echo json_encode($working_day); ?>;
    working_day_target = <?php echo json_encode($working_day_target); ?>;

    for (var i = 0; i < working_day.length; i++) {
        labels.push(working_day[i].jornada_programa);
        count.push(working_day[i].count);
    }

    for (var i = 0; i < working_day_target.length; i++) {
        sum_target.push(working_day_target[i].suma);
    }

	var ctx = document.getElementById("myChartWorkingDay").getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: labels,
	        datasets: [{
	            label: 'Cantidad de matriculados',
	            data: count,
	            backgroundColor: 'rgba(54, 162, 235, 0.2)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        },
	        {
	            label: 'Meta 2018',
	            data: sum_target,
	            backgroundColor: 'rgba(54, 162, 235, 0.6)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        }]
	    },
	    options: {
	    	animation: {
		    	onProgress: drawBarValues,
		    	onComplete: drawBarValues
		    },
		    hover: { 
		    	animationDuration: 0 
		    },
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    fontSize: 9
	                }
	            }],
	            xAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    autoSkip: false,

	                }
	            }]
	        }
	    }
	});

	//gráfico de alumnos por origen (nuevos, antiguos)
    older = <?php echo json_encode($older); ?>;
    older_new_target = <?php echo json_encode($older_new_target); ?>;

	var etiquetas = [], suma = [], count = [];

    for (var i = 0; i < older_new_target.length; i++) {
        etiquetas.push(older_new_target[i].origen);
        suma.push(older_new_target[i].suma);
    }

    for (var i = 0; i < older.length; i++) {
        count.push(older[i].count);
    }

    var ctx = document.getElementById("myChartOrlderNew").getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: etiquetas,
	        datasets: [{
	            label: 'Cantidad de matriculados',
	            data: count,
	            backgroundColor: 'rgba(54, 162, 235, 0.2)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        },
	        {
	            label: 'Meta 2018',
	            data: suma,
	            backgroundColor: 'rgba(54, 162, 235, 0.6)',
	            borderColor: 'rgba(54, 162, 235, 1)',
	            borderWidth: 1
	        }]
	    },
	    options: {
	    	animation: {
		    	onProgress: drawBarValues,
		    	onComplete: drawBarValues
		    },
		    hover: { 
		    	animationDuration: 0 
		    },
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    fontSize: 9
	                }
	            }],
	            xAxes: [{
	                ticks: {
	                    beginAtZero:true,
	                    autoSkip: false,

	                }
	            }]
	        }
	    }
	});

</script>

@endpush

@push('datatable')
  <script>
    $(document).ready(function(){

        $('#targetTable').DataTable({

            responsive: true,
            processing: true,

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "search": "Filtrar:",
                "zeroRecords": "No encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(Filtrando _MAX_ registros)",
                "paginate": {
                    "previous": "Página anterior",
                    "next": "Página siguiente"
                  },
            },

            fixedColumns: true,

        });

    });


</script>
@endpush