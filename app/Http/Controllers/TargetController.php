<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Target;
use App\Registered;
use App\Retention;
use DB;
use Illuminate\Http\Request;


class TargetController extends Controller
{
    public function index() {

    	$metas = DB::table('target')->get();

        return view('metas.index', compact('metas'));    
      
    }

    public function FilterByHeadquarter(Request $request) {

    	if($request->has('cod')) {

			$registered_headquarter = DB::select("select jefe_carrera, count(registered.id) as count from registered where registered.cod_sede_anterior = $request->cod group by jefe_carrera");

			//hacer la aclaración de los jefes de carrera que aparecen al hacer la consulta sobre el código de carrera anterior.

    		$filter = DB::table('target')
    				->select(DB::raw('sum(meta_alumnos) as suma, jefe_carrera, sede'))
    				->where('cod_sede','=', $request->cod)
    				->groupBy('jefe_carrera','sede')
	    			->get();

	    	$habilitados_retention = DB::table('retention')
    				->select(DB::raw('IFNULL(count(retention.id),0) as count, jefe_carrera, sede'))
    				->where('status_habilitado','=', 'HABILITADO')
    				->where('cod_sede','=', $request->cod)
    				->groupBy('jefe_carrera','sede')
	    			->get();

            $chiefs = DB::table('retention')
                    ->select(DB::raw('jefe_carrera, sede'))
                    ->where('cod_sede','=', $request->cod)
                    ->groupBy('jefe_carrera','sede')
                    ->get();

            if(count($habilitados_retention) == count($chiefs)){                
    		    foreach ($registered_headquarter as $index => $register) {
                    $data[] = [
                        'count' => $register->count,
                        'jefe_carrera' => $register->jefe_carrera,
                        'suma' => $filter[$index]->suma,
    		    		'count_habilitados' => $habilitados_retention[$index]->count
    		    	];
    		    }
            }
            else {
                foreach ($registered_headquarter as $index => $register) {
                    $data[] = [
                        'count' => $register->count,
                        'jefe_carrera' => $register->jefe_carrera,
                        'suma' => $filter[$index]->suma,
                        'count_habilitados' => '1'
                    ];
                }
            }

            //return $data;
		    
	    	$name = DB::table('target')
    				->select(DB::raw('sede'))
    				->distinct()
    				->where('cod_sede','=', $request->cod)
	    			->get();

	    	//consulta por jornada
	    	$working_day = DB::table('registered')
    				->select(DB::raw('jornada_programa, count(jornada_programa) as count'))
    				->distinct()
    				->where('cod_sede_anterior','=', $request->cod)
    				//->where('jornada_programa','!=', 'M')
    				//->where('jornada_programa','!=', 'ONL')
    				->groupBy('jornada_programa')
    				->orderBy('jornada_programa')
	    			->get();

	    	$working_day_target = DB::table('target')
    				->select(DB::raw('jornada, sum(meta_alumnos) as suma'))
    				->distinct()
    				->where('cod_sede','=', $request->cod)
    				->groupBy('jornada')
    				->orderBy('jornada')
	    			->get();

	    	$older = DB::table('registered')
	    			->select(DB::raw('estado_origen, count(estado_origen) as count'))
	    			->where('cod_sede_anterior','=', $request->cod)
	    			->groupBy('estado_origen')
	    			->get();

	    	$older_new_target = DB::table('target')
    				->select(DB::raw('origen, sum(meta_alumnos) as suma'))
    				->where('cod_sede','=', $request->cod)
    				->groupBy('origen')
	    			->get();

	    	return view('sede.index', compact('filter','name','data','working_day','working_day_target','older','older_new_target'));  
    	}

    	else {
    		return redirect('/');
    	}
      
    }

}
