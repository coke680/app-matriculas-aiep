<?php

namespace App\Http\Controllers;

use App\Registered;
use App\Target;
use App\Retention;
use DB;
use Illuminate\Http\Request;


class RegisteredController extends Controller
{
    public function index() {

	    $registered = DB::table('registered')
	    			->select(DB::raw('count(registered.id) as count'))	    			
	    			->get();

	    $target = DB::table('target')
	    			->select(DB::raw('sum(meta_alumnos) as sum'))
	    			->get();

	    //consulta a los matriculados por sede
	    $registered_by_headquarter = Registered::selectRaw('count(registered.id) as count,
	    								sede_anterior, cod_sede_anterior')	    							
	    							->groupBy('sede_anterior', 'cod_sede_anterior')
	    							->orderBy('sede_anterior', 'asc')
	    							->limit(30)
	    							->get();

	    $codigos = [];

	    //asignar códigos de sede a un array
	    foreach ($registered_by_headquarter as $register)
	    	$codigos[] = $register->cod_sede_anterior;

	    //comparar los matriculados con las metas
	    $target_headquarter  = Target::selectRaw('sum(meta_alumnos) as suma')
	    			->orderBy('sede', 'asc')
	    			->groupBy('sede')
					->whereIn('target.cod_sede', $codigos)
					->limit(30)
	    			->get();

	    $retention_headquarter  = Retention::selectRaw('count(retention.id) as count')
	    			->where('status_habilitado','=', 'HABILITADO')
	    			->orderBy('sede', 'asc')
	    			->groupBy('sede')
					->whereIn('retention.cod_sede', $codigos)
					->limit(30)
	    			->get();

	    //crear array para recorrer en la vista			
	    $data = [];

	    foreach ($registered_by_headquarter as $index => $register) {

	    	$data[] = [
	    		'cod_sede' => $register->cod_sede_anterior,
	    		'count' => $register->count,
	    		'sede' => $register->sede_anterior,
	    		'suma' => $target_headquarter[$index]->suma,
	    		'count_habilitado' => $retention_headquarter[$index]->count  
	    	];
	    }

	    //calcular porcentaje hasta el momento
	    $total_target = $target[0]->sum;
	    $total_registered = $registered[0]->count;
	    $percentage = (($total_registered * 100) / $total_target);
	    $percentage = number_format((float)$percentage, 2, '.', '');

        return view('matriculados.index', compact('total_registered', 'percentage', 'target_headquarter','registered_by_headquarter', 'data'));      	

    }

    public function dataChart() {

    	$registered = DB::table('registered')
	    			->select(DB::raw('count(registered.id) as count, sede_anterior, cod_sede_anterior'))  			
	    			->limit(22)
	    			->groupBy('sede_anterior','cod_sede_anterior')
	    			->orderBy('sede_anterior','asc')
	    			->get();

	    $codigos = [];

	    foreach ($registered as $register)
	    	$codigos[] = $register->cod_sede_anterior;

	    $target = DB::table('target')
	    			->select(DB::raw('sum(meta_alumnos) as suma, sede, cod_sede'))	    			
	    			->whereIn('target.cod_sede', $codigos )
	    			->limit(22)
	    			->groupBy('sede','cod_sede')
	    			->orderBy('sede','asc')
	    			->get();

	    //crear array para recorrer en la vista			
	    $data = [];

	    foreach ($registered as $index => $register) {
	    	$data[] = [
	    		'cod_sede' => $register->cod_sede_anterior,
	    		'count' => $register->count,
	    		'sede' => $target[$index]->sede,
	    		'suma' => $target[$index]->suma 
	    	];
	    }

	    return compact('data');

    }

    public function dataChartFaculty() {

    	$registered_faculty = DB::table('registered')
	    			->select(DB::raw('count(registered.id) as count, escuela_anterior'))	    			
	    			->groupBy('escuela_anterior')
	    			->orderBy('escuela_anterior','asc')
	    			->get();

		$target_faculty = DB::table('target')
	    			->select(DB::raw('sum(meta_alumnos) as suma, escuela'))	    			
	    			->groupBy('escuela')
	    			->orderBy('escuela','asc')
	    			->get();

		return compact('registered_faculty','target_faculty');

    }

    public function dataChartDate() {

    	return $registered = DB::table('registered')
	    			->select(DB::raw('count(registered.id) as count, fecha'))	    			
	    			->groupBy('fecha')
	    			->orderBy('fecha','asc')
	    			->take(30)
	    			->get();

    }

    public function dataChartWorkingDay() {

    	$working_day = DB::table('registered')
	    			->select(DB::raw('count(jornada_programa) as suma_jornada, jornada_programa'))    			
	    			->groupBy('jornada_programa')
	    			->orderBy('jornada_programa')
	    			->get();

	    $working_day_target = DB::table('target')
	    			->select(DB::raw('sum(meta_alumnos) as suma_jornada, jornada'))	    			
	    			->groupBy('jornada')
	    			->orderBy('jornada')
	    			->get();


	    return compact('working_day','working_day_target');

    }

    public function dataChartOlderNew() {

    	$older = DB::table('registered')
	    			->select(DB::raw('estado_origen, count(estado_origen) as count'))
	    			->groupBy('estado_origen')
	    			->get();

    	$older_new_target = DB::table('target')
					->select(DB::raw('origen, sum(meta_alumnos) as suma'))
					->groupBy('origen')
    				->get();


	    return compact('older','older_new_target');

    }

}


	    