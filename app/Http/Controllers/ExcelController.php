<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registered;
use App\Retention;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{

    public function export() {

    	Excel::create('Registro de Rematriculados', function($excel) {
 
            $excel->sheet('Datos', function($sheet) {
 
                $registered = Registered::select('matricula', 'origen', 'rutalumno', 'nombrealumno', 'cod_carrera', 'carrera', 'facultad', 'fecha', 'cod_sede', 'sede_carrera', 'jornada', 'usuariomat', 'rutaceptante', 'nomaceptante', 'tipo_arancel', 'agno_ingreso', 'cod_carrera_anterior','sede_anterior','cod_sede_anterior')->get();
 
                $sheet->fromArray($registered);
 
            });
        })->export('xls');
	}

	public function exportNoMatriculados() {

		Excel::create('Registro de No Matriculados', function($excel) {
 
            $excel->sheet('Datos', function($sheet) {
 
                $retention = Retention::select('matricula', 'rutalumno', 'nombre_alumno', 'cod_carrera', 'carrera', 'sede', 'cod_sede','facultad', 'tipo_carrera', 'jornada', 'tipo_beneficio', 'tipo_estudiante', 'estado_matricula', 'jefe_carrera', 'aprobacion_semestre_uno', 'rango_aprobacion as rango_aprobacion_uno','asistencia_uno as asistencia_semestre_uno','rango_asistencia_uno','deuda_total','mora','codigo_bloqueo','estado_final','status_habilitado','fono_1','fono_2','fono_3','fono_4','fono_5','fono_6','fono_7')->get();
 
                $sheet->fromArray($retention);
 
            });
        })->export('xls');
	}
}
