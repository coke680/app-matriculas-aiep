<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registered extends Model
{
    protected $fillable = ['matricula','origen','rutalumno','nombrealumno','cod_carrera','carrera','facultad','fecha','sede_carrera','jornada','usuariomat','rutaceptante','nomaceptante','tipo_arancel','agno_ingreso','cod_carrera_anterior'];

    protected $table = 'registered';
}
